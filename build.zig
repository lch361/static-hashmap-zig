const std = @import("std");
const Build = std.Build;

pub fn build(b: *Build) !void {
    _ = b.addModule("static-hashmap", .{
        .source_file = Build.LazyPath.relative("src/lib.zig"),
    });
}
