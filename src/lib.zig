const std = @import("std");
const expectEqual = std.testing.expectEqual;
const expect = std.testing.expect;

/// Defines behaviour of K as a key type for `StaticHashmap`
pub fn HashableKey(comptime K: type) type {
    return struct {
        hash: fn(K) usize,
        cmp: fn(K, K) bool,
    };
}

/// A hashmap without dynamic memory allocations:
/// initialize in comptime with `init(...)`, use in runtime with `get(...)`.
pub fn StaticHashmap(comptime K: type, comptime V: type, comptime H: HashableKey(K)) type {
    return struct {
        const Self = @This();

        const Node = struct {
            key: K,
            value: V,
        };

        elements: []const []const Self.Node,

        pub fn init(comptime elements: []const struct { K, V }) Self {
            const result = comptime blk: {
                const len: usize = elements.len * 2 + 1;
                var result: [len][]const Self.Node = [_][]const Self.Node{ &.{} } ** len;
                for (elements) |i| {
                    const node = Self.Node { .key = i[0], .value = i[1] };
                    const index = H.hash(node.key) % len;
                    result[index] = result[index] ++ &[_]Self.Node{ node };
                }
                break :blk &result;
            };
            return Self {
                .elements = result,
            };
        }

        pub fn get(self: Self, key: K) ?*const V {
            const index: usize = H.hash(key) % self.elements.len;
            const subslice = self.elements[index];
            for (subslice) |*i| {
                if (H.cmp(i.key, key)) {
                    return &i.value;
                }
            }
            return null;
        }
    };
}

fn str_hash(key: []const u8) usize {
    return @intCast(std.hash.Wyhash.hash(1283, key));
}

fn str_cmp(a: []const u8, b: []const u8) bool {
    return std.mem.eql(u8, a, b);
}

const HASH_STR = HashableKey([]const u8) {
    .hash = str_hash,
    .cmp = str_cmp,
};

test "Empty" {
    const map = StaticHashmap([]const u8, i32, HASH_STR).init(&.{});
    try expectEqual(map.get("aaaa"), null);
}

test "Not empty" {
    const map = StaticHashmap([]const u8, i32, HASH_STR).init(&.{
        .{ "abc", 13 },
        .{ "meaning_of_life", 42 },
        .{ "kromer", 1997 },
        .{ "rofl", 420 },
    });

    try expectEqual(map.get("aaaa"), null);
    try expectEqual(map.get("abc").?.*, 13);
    try expectEqual(map.get("meaning_of_life").?.*, 42);
    try expectEqual(map.get("kromer").?.*, 1997);
    try expectEqual(map.get("rofl").?.*, 420);
    try expectEqual(map.get("ro"), null);
}
